#include "control.h"
#include "trajectory_generation.h"
#include <iostream>
#include <fstream>
using namespace std;

int main(){
  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial position 
  //      q   = M_PI_2, M_PI_4
  // and  
  //      l1  = 0.4, l2 = 0.5
  // simulate a motion achieving 
  //      Xf  = 0.0, 0.6

  Eigen::Vector2d qi(M_PI_2, M_PI_4);
  Eigen::Vector2d xf(0.0,0.6);
  Eigen::Vector2d X,Xd,xi,dXd,q,dqd;
  
  Eigen::Matrix2d Ji,J;

  double dtf = 10;
  double t =0.0;

  RobotModel RM(0.4,0.5);
  Controller JC(RM);

  RM.FwdKin(xi,Ji,qi); // computation of initial parameters

  Point2Point Trajectory(xi,xf,dtf); // generation of polynomial of desired trajectory
  q = qi;

  Eigen::Vector2d real_X = xi;

  ofstream myfile;
  myfile.open("control_output.csv"); // .csv file to see if we can attain desired position and also plot velocity
  myfile << "Xd" <<","<< "Yd" <<","<< "Vx" <<","<< "Vy"  <<","<< "real_X" <<","<< "real_Y" <<","<<"t" <<"\n";
  //myfile << "Xd" <<","<< "real_X" <<","<< "Yd" <<","<< "real_Y" <<","<< "Vx" <<","<< "Vy"  <<","<< "q" <<","<< "dqd" <<","<<"t" <<"\n";


  while(t < dtf)
  {
    Xd = Trajectory.X(t); //computation of desired current position
    dXd = Trajectory.dX(t); //computation of desired current speed
    //get_q();
    dqd = JC.Dqd(q,Xd,dXd);
    
    q+= dqd*0.01; // simulation of perfect robot
    //send_dqd;

    real_X[0] += dXd[0] * 0.01;
    real_X[1] += dXd[1] * 0.01;

    myfile << Xd[0] <<","<< Xd[1] <<","<< dXd[0] <<","<< dXd[1] <<","<< real_X[0] <<","<< real_X[1] <<","<< t <<"\n";
    //myfile << 1 <<","<< 1 <<","<< 1 <<","<< 1 <<","<< 1 <<","<< 1 <<","<< 1 <<"\n";
    t+=0.01;
    //myfile << Xd[0] <<","<< real_X[0] <<","<< Xd[1] <<","<< real_X[1] <<","<< dXd[0] <<","<< dXd[1]  <<","<< q <<","<< dqd <<","<< t <<"\n";
  }
  RM.FwdKin(X,J,q);
  cout << "X en fin de boucle  = " << X <<endl;
  myfile.close();
}