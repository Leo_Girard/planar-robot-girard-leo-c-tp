#include "kinematic_model.h"
#include <iostream>

using namespace std;

int main(){
  // Compute the forward kinematics and jacobian matrix for 
  //      q =  M_PI/3.0,  M_PI_4
  //Eigen::Vector2d q(M_PI/3.0,  M_PI_4);
  Eigen::Vector2d q(0,  M_PI/2.0);
  Eigen::Vector2d X,X2;
  Eigen::Matrix2d J,J2;
  RobotModel Test(0.5,0.5);

  Test.FwdKin(X,J,q);
  cout << "X = " << X << endl;
  cout << "J = " << J << endl;

  // For a small variation of q, compute the variation on X and check dx = J . dq 
  Eigen::Vector2d dq (0.01,0.01);
  Eigen::Vector2d q2 = q + dq;
  Test.FwdKin(X2,J2,q2);

  Eigen::Vector2d Truedx = X2 - X;
  Eigen::Vector2d dx = J * dq;

  cout << "Truedx = " << Truedx << endl;
  cout << "dx = " << dx << endl;

  //verif du modele géometrique et de la jacobienne ok
}
