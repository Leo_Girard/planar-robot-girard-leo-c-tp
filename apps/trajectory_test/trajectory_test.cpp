#include "trajectory_generation.h"
#include <iostream>
#include <fstream>
//#include "eigen3/Eigen/Dense"
using namespace std;
int main(){

  // Compute the trajectory for given initial and final positions. 
  // Display some of the computed points
  
  // trajectory_test > output.csv
  // Plot Juggler
  Eigen::Vector2d xi(0.0,0.0);
  Eigen::Vector2d xf(3.0,9.0);
  Eigen::Vector2d X,V,last_X,TrueV(0.0,0.0);
  double dt = 3.0;
  Point2Point test(xi,xf,dt);
  
  ofstream myfile;
  myfile.open("output.csv"); // .csv file to see if we can attain desired position and also plot velocity
  myfile << "xd" <<","<< "yd" <<","<< "Vx" <<","<< "Vy" <<","<< "TrueVx" <<","<< "TrueVy" <<","<< "t" <<"\n";

  for(double i =0.01; i<dt; i+= 0.01)
  {
    
    X = test.X(i); // computation of current time desired position
    V = test.dX(i);
    
    // add [Xn - X(n-1)]/dt
    TrueV[0] = (X[0]-last_X[0])/0.01; // computation of true speed
    TrueV[1] = (X[1]-last_X[1])/0.01;
    

    myfile << X[0] <<","<< X[1] <<","<< V[0] <<","<< V[1] <<","<< TrueV[0] <<","<< TrueV[1] <<","<< i <<"\n";

    cout << "X = " << X << endl;
    cout << "V = " << V << endl;
    last_X = X; //save of current position for next difference
    
  }
  myfile.close();

  // Check numerically the velocities given by the library 
  // Check initial and final velocities
}