#include "control.h"
#include <iostream>
Controller::Controller(RobotModel &rmIn):
  model  (rmIn)
{
}

Eigen::Vector2d Controller::Dqd ( 
          const Eigen::Vector2d & q,
          const Eigen::Vector2d & xd,
          const Eigen::Vector2d & Dxd_ff
                      )
{  
  //TODO Compute joint velocities able to track the desired cartesian position
  Eigen::Vector2d e,XK1,dqd;
  Eigen::Matrix2d Jinv;

  model.FwdKin(X,J,q); // computation of actual pose and current Jacobian of the robot
  e = xd-X; // computation of position error
  XK1 = kp * e; // transformation into a velocity
  Jinv = J.inverse(); // inversion of jacobian matrix
  dqd = Jinv * (XK1 + Dxd_ff); //computation of desired joint velocity
  return dqd;
}

