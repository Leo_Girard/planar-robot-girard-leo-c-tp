#include "kinematic_model.h"

RobotModel::RobotModel(const double &l1In, const double &l2In):
  l1  (l1In),
  l2  (l2In)
{};

void RobotModel::FwdKin(Eigen::Vector2d &xOut, Eigen::Matrix2d &JOut, const Eigen::Vector2d & qIn){
  // TODO Implement the forward and differential kinematics
  xOut[0] = l1*cos(qIn[0]) + l2*cos(qIn[0]+qIn[1]); //pos x
  xOut[1] = l1*sin(qIn[0]) + l2*sin(qIn[0]+qIn[1]);//pos y

  JOut(0,0) = -l1*sin(qIn[0]) - l2*sin(qIn[0]+qIn[1]); // dx/dq1
  JOut(1,0) = l1*cos(qIn[0]) + l2*cos(qIn[0]+qIn[1]); // dy/dq1
  JOut(0,1) = -l2*sin(qIn[0]+qIn[1]); // dx/dq2
  JOut(1,1) = l2*cos(qIn[0]+qIn[1]); // dx/dq2
}