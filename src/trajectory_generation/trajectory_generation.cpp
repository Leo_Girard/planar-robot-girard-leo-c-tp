#include "trajectory_generation.h"

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
  double D = pfIn - piIn; // computing the diference between final and initial position;
  
  // Computing polynomial coefficients
  a[0] = piIn; 
  a[1] = 0;
  a[2] = 0;
  a[3] = (10 / pow(DtIn, 3)) * D;
  a[4] = (-15 / pow(DtIn,4)) * D;
  a[5] = (6 / pow(DtIn,5)) * D;
  //cout << "pf = " << pf << endl; // for testing purposes
  //cout << " a4 = " << a[4] << endl; // for testing purposes
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients
  double D = pfIn - piIn; // computing the diference between final and initial position;
  
  // Computing polynomial coefficients
  a[0] = piIn; 
  a[1] = 0;
  a[2] = 0;
  a[3] = (10 / pow(DtIn, 3)) * D;
  a[4] = (-15 / pow(DtIn,4)) * D;
  a[5] = (6 / pow(DtIn,5)) * D;
};

const double  Polynomial::p     (const double &t){ // is it the current position(call dkm) or the desired position(compute with polynomial)?
  //TODO compute position
  double pd;

  pd = pi + a[0]+ a[1]*t + a[2]*pow(t,2) + a[3]*pow(t,3) + a[4]*pow(t,4) + a[5]*pow(t,5); // computing the next position of trajectory
  return pd;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity
  double vd;

  vd = a[1] + 2*a[2]*t + 3*a[3]*pow(t,2) + 4*a[4]*pow(t,3) + 5*a[5]*pow(t,4); // computing velocity by the derivative of the position
  return vd;
};

Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn){
  //TODO initialize object and polynomials
  polx.update(xi[0],xf[0],DtIn); //initialization of x polynomial
  poly.update(xi[1],xf[1],DtIn); //initialization of y polynomial
  Dt = DtIn;
  
}

Eigen::Vector2d Point2Point::X(const double & time){ // use Polynomial::p to compute the positions in x and y
  //TODO compute cartesian position
  Eigen::Vector2d XD;

  XD[0] = polx.p(time); // compute desired x coordinate
  XD[1] = poly.p(time); // compute desired y coordinate

  return XD;
}

Eigen::Vector2d Point2Point::dX(const double & time){// use Polynomial::dp to compute the velocities in x and y
  //TODO compute cartesian velocity
  Eigen::Vector2d VD;

  VD[0] = polx.dp(time); // compute desired x velocity
  VD[1] = poly.dp(time); // compute desired y velocity

  return VD;
}